# Changelog
All notable changes to this project will be documented in this file.

## [2.0.0] - 2023-11-14

### Added

- Game rule: `dreamTicks`
  - sets the amount of sleep ticks required to teleport to Dream dimension
- Independent potion effects between dimensions

### Changed

- `Dream` dimension generation
  - added superflat layers
  - no longer spawns structures by default
- Nbt tags

### Removed

- Temporary floor creation in `Dream` dimension
- Unbreakable world floor in `Dream` dimension

### Fixed

- Player death in `Dream` dimension
  - Restores health to max and prevents death
  - Relocates player if death happens beneath the world

## [1.2.0] - 2023-11-09

### Added

- Separate Ender Chest inventory for `Dream` dimension

## [1.1.2] - 2023-03-17

### Fixed

- Disabled use of Ender Chest while in `Dream`

## [1.1.1] - 2022-12-27

### Fixed

- Bug disabling skipping the night when sleeping with item in hand
- Game rule category formatting in create world screen

## [1.1.0] - 2022-12-14

### Added

- Remove sleep limitations when sleeping in `Dream`

## [1.0.0] - 2022-12-13

### Added

- Add death protection while in `Dream`
- Add game rule `createDreamFloor`

## [0.2.0] - 2022-12-13

### Added

- Add floor generation when entering `Dream` dimension with non-persistent leaves, if landing on air

### Changed

- Move initial position in `Dream` dimension up from `Y:0` to `Y:64`
- Remove fixed time in `Dream` dimension; time now passes like the `Overworld`

### Fixed

- Fix biome settings in `dream.json` file

## [0.1.0] - 2022-12-10

### Added

- Add `Dream` dimension