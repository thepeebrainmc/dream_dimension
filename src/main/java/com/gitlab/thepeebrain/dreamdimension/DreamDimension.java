package com.gitlab.thepeebrain.dreamdimension;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.entity.event.v1.EntitySleepEvents;
import net.fabricmc.fabric.api.entity.event.v1.ServerLivingEntityEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.minecraft.registry.RegistryKey;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.minecraft.registry.RegistryKeys.WORLD;

public class DreamDimension
		implements ModInitializer
{
	public static final String MOD_ID = "dream_dimension";
	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	@Override
	public void onInitialize()
	{
		GameRules.init();

		ServerLifecycleEvents.SERVER_STARTED.register(server -> {
			var gameRules = server.getGameRules();
			LOGGER.info("requireEmptyHands: " + gameRules.getBoolean(GameRules.REQUIRE_EMPTY_HANDS));
			LOGGER.info("dreamTicks: " + gameRules.getInt(GameRules.DREAM_TICKS));
		});

		ServerLivingEntityEvents.ALLOW_DEATH.register((entity, damageSource, damageAmount)
				-> !(entity instanceof ServerPlayerEntity player)
				|| player.getDreamDimensionTeleporter().onPlayerDeath()
		);

		EntitySleepEvents.ALLOW_SETTING_SPAWN.register((player, sleepingPos) -> !Dimensions.isDream(player.getWorld()));
		EntitySleepEvents.ALLOW_SLEEP_TIME.register((player, sleepingPos, vanillaResult) ->
				Dimensions.isDream(player.getWorld()) ? ActionResult.SUCCESS : ActionResult.PASS);
		EntitySleepEvents.ALLOW_NEARBY_MONSTERS.register((player, sleepingPos, vanillaResult) ->
				Dimensions.isDream(player.getWorld()) ? ActionResult.SUCCESS : ActionResult.PASS);
	}

	public static class Dimensions
	{
		public static final Identifier DREAM = new Identifier(MOD_ID, "dream");
		public static final Identifier OVERWORLD = new Identifier("minecraft", "overworld");

		public static final RegistryKey<World> DREAM_KEY = RegistryKey.of(WORLD, DREAM);
		public static final RegistryKey<World> OVERWORLD_KEY = World.OVERWORLD;

		public static boolean isDream(World world)
		{
			return world.getRegistryKey().getValue().equals(DREAM);
		}

		public static boolean isOverworld(World world)
		{
			return world.getRegistryKey().getValue().equals(OVERWORLD);
		}
	}
}
