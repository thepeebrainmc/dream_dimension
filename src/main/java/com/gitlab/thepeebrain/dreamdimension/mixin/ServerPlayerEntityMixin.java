package com.gitlab.thepeebrain.dreamdimension.mixin;

import com.gitlab.thepeebrain.dreamdimension.DreamDimensionTeleporter;
import com.gitlab.thepeebrain.dreamdimension.DreamDimensionTeleporterGetter;
import com.mojang.authlib.GameProfile;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerPlayerEntity.class)
public abstract class ServerPlayerEntityMixin
		extends PlayerEntity
		implements DreamDimensionTeleporterGetter
{
	@SuppressWarnings("ConstantConditions")
	private final DreamDimensionTeleporter dimensionTeleporter =
			new DreamDimensionTeleporter((ServerPlayerEntity)(Object)this);

	public ServerPlayerEntityMixin(World world, BlockPos pos, float yaw, GameProfile gameProfile)
	{
		super(world, pos, yaw, gameProfile);
	}

	// ============================================================================================================== //

	@Inject(at = @At("TAIL"), method = "playerTick()V")
	public void playerTick(CallbackInfo info)
	{
		dimensionTeleporter.onPlayerTick();
	}

	@Inject(at = @At("TAIL"), method = "writeCustomDataToNbt(Lnet/minecraft/nbt/NbtCompound;)V")
	public void writeNbt(NbtCompound nbt, CallbackInfo info)
	{
		dimensionTeleporter.writeNbt(nbt);
	}

	@Inject(at = @At("HEAD"), method = "readCustomDataFromNbt(Lnet/minecraft/nbt/NbtCompound;)V")
	public void readNbt(NbtCompound nbt, CallbackInfo info)
	{
		dimensionTeleporter.readNbt(nbt);
	}

	@Override
	public DreamDimensionTeleporter getDreamDimensionTeleporter()
	{
		return dimensionTeleporter;
	}
}
