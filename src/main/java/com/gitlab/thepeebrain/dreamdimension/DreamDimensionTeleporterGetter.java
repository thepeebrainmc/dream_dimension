package com.gitlab.thepeebrain.dreamdimension;

public interface DreamDimensionTeleporterGetter
{
	default DreamDimensionTeleporter getDreamDimensionTeleporter()
	{
		return null;
	}
}
