package com.gitlab.thepeebrain.dreamdimension;

import com.gitlab.thepeebrain.dreamdimension.DreamDimension.Dimensions;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtList;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.GameMode;

public class DimensionData
{
	private final Identifier dimension;

	private final ServerPlayerEntity player;

	private NbtCompound data;

	public DimensionData(Identifier dimension, ServerPlayerEntity player)
	{
		this.dimension = dimension;
		this.player = player;
		this.data = new NbtCompound();

		if(dimension == Dimensions.DREAM)
		{
			data.putInt("playerGameType", GameMode.CREATIVE.getId());
			data.putFloat("Health", 20);
			var pos = findValidSpawnPos(0, 0);
			data.putInt("SpawnX", pos.getX());
			data.putInt("SpawnY", pos.getY());
			data.putInt("SpawnZ", pos.getZ());
		}
	}

	public BlockPos getSpawnPos()
	{
		return new BlockPos(
				data.getInt("SpawnX"),
				data.getInt("SpawnY"),
				data.getInt("SpawnZ")
		);
	}

	public BlockPos findValidSpawnPos(int x, int z)
	{
		var world = player.getServerWorld();
		world.getHeight();
		var topY = world.getTopY();
		var bottomY = world.getBottomY();
		var y = topY;
		while(y > bottomY)
		{
			if(world.isTopSolid(new BlockPos(x, y, z), player))
			{
				return new BlockPos(x, y + 1, z);
			}

			y--;
		}

		return new BlockPos(x, topY, z);
	}

	// ============================================================================================================== //

	public void save()
	{
		data = new NbtCompound();

		var pos = player.getBlockPos();
		if(pos.getY() <= player.getWorld().getBottomY())
		{
			pos = findValidSpawnPos(pos.getX(), pos.getZ());
		}
		data.putInt("SpawnX", pos.getX());
		data.putInt("SpawnY", pos.getY());
		data.putInt("SpawnZ", pos.getZ());

		data.putInt("playerGameType", player.interactionManager.getGameMode().getId());

		data.put("Inventory", player.getInventory().writeNbt(new NbtList()));
		data.put("EnderItems", player.getEnderChestInventory().toNbtList());

		var hungerManager = player.getHungerManager();
		data.putFloat("foodExhaustionLevel", hungerManager.getExhaustion());
		data.putInt("foodLevel", hungerManager.getFoodLevel());
		data.putFloat("foodSaturationLevel", hungerManager.getSaturationLevel());
		data.putFloat("Health", player.getHealth());

		data.putInt("XpLevel", player.experienceLevel);
		data.putFloat("XpP", player.experienceProgress);
		data.putInt("XpTotal", player.totalExperience);

		if(player.getActiveStatusEffects().isEmpty())
		{
			data.remove("active_effects");
		}
		else
		{
			var nbtList = new NbtList();
			for(var statusEffectInstance : player.getActiveStatusEffects().values())
			{
				nbtList.add(statusEffectInstance.writeNbt(new NbtCompound()));
			}
			data.put("active_effects", nbtList);
		}
	}

	public void load()
	{
		player.teleport(
				data.getInt("SpawnX"),
				data.getInt("SpawnY"),
				data.getInt("SpawnZ")
		);

		player.changeGameMode(GameMode.getOrNull(data.getInt("playerGameType")));

		player.getInventory().readNbt(data.getList("Inventory", NbtElement.COMPOUND_TYPE));
		player.getEnderChestInventory().readNbtList(data.getList("EnderItems", NbtElement.COMPOUND_TYPE));

		var hungerManager = player.getHungerManager();
		hungerManager.setExhaustion(data.getFloat("foodExhaustionLevel"));
		hungerManager.setFoodLevel(data.getInt("foodLevel"));
		hungerManager.setSaturationLevel(data.getFloat("foodSaturationLevel"));

		player.setExperienceLevel(data.getInt("XpLevel"));
		player.experienceProgress = data.getFloat("XpP");
		player.totalExperience = data.getInt(("XpTotal"));

		player.clearStatusEffects();
		if(data.contains("active_effects", NbtElement.LIST_TYPE))
		{
			var nbtList = data.getList("active_effects", NbtElement.COMPOUND_TYPE);
			for(var i = 0; i < nbtList.size(); ++i)
			{
				var nbtStatusEffectInstance = nbtList.getCompound(i);
				var statusEffectInstance = StatusEffectInstance.fromNbt(nbtStatusEffectInstance);
				if(statusEffectInstance != null)
				{
					player.addStatusEffect(statusEffectInstance);
				}
			}
		}
	}

	// ============================================================================================================== //

	public void readNbt(NbtCompound nbt)
	{
		var nbtCompound = nbt.getCompound(DreamDimension.MOD_ID);
		if(nbtCompound.contains(dimension.toString(), NbtElement.COMPOUND_TYPE))
		{
			data = nbtCompound.getCompound(dimension.toString());
		}
	}

	public void writeNbt(NbtCompound nbt)
	{
		if(dimension.toString().equals(nbt.getString("Dimension")))
		{
			save();
		}

		var key = DreamDimension.MOD_ID;
		var nbtCompound = nbt.getCompound(key);
		if(!nbt.contains(key, NbtElement.COMPOUND_TYPE))
		{
			nbt.put(key, nbtCompound);
		}

		nbtCompound.put(dimension.toString(), data.copy());
	}
}
