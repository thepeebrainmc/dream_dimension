package com.gitlab.thepeebrain.dreamdimension;

import com.gitlab.thepeebrain.dreamdimension.DreamDimension.Dimensions;
import net.minecraft.block.BedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.registry.RegistryKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

import java.util.Objects;

public class DreamDimensionTeleporter
{
	private final ServerPlayerEntity player;

	private final DimensionData dreamData;
	private final DimensionData overworldData;

	// CONSTRUCTOR ================================================================================================== //

	public DreamDimensionTeleporter(ServerPlayerEntity player)
	{
		this.player = player;
		this.dreamData = new DimensionData(Dimensions.DREAM, player);
		this.overworldData = new DimensionData(Dimensions.OVERWORLD, player);
	}

	// ============================================================================================================== //

	//Returns true if death should be allowed
	//Only returns true when death happens outside of Dream dimension
	public boolean onPlayerDeath()
	{
		if(!Dimensions.isDream(player.getWorld()))
		{
			return true;
		}

		player.setHealth(player.getMaxHealth());

		//Relocate player if below world bottom
		if(player.getPos().getY() <= player.getWorld().getBottomY())
		{
			var pos = dreamData.getSpawnPos();
			pos = dreamData.findValidSpawnPos(pos.getX(), pos.getZ());
			player.teleport(pos.getX(), pos.getY(), pos.getZ());
		}

		return false;
	}

	public void onPlayerTick()
	{
		if(!hasSleptLongEnough() || !canTeleport())
		{
			return;
		}

		var world = player.getWorld();
		if(Dimensions.isOverworld(world))
		{
			teleport(Dimensions.DREAM_KEY, overworldData, dreamData);
		}
		else if(Dimensions.isDream(world))
		{
			teleport(Dimensions.OVERWORLD_KEY, dreamData, overworldData);
		}
	}

	public boolean hasSleptLongEnough()
	{
		return player.isSleeping() &&
				player.getSleepTimer() >= player.getWorld().getGameRules().getInt(GameRules.DREAM_TICKS);
	}

	public boolean canTeleport()
	{
		return !player.getWorld().getGameRules().getBoolean(GameRules.REQUIRE_EMPTY_HANDS)
				|| (player.getStackInHand(Hand.MAIN_HAND).isEmpty() && player.getStackInHand(Hand.OFF_HAND).isEmpty());
	}

	// ============================================================================================================== //

	private void teleport(RegistryKey<World> key, DimensionData current, DimensionData target)
	{
		MinecraftServer server = Objects.requireNonNull(player.getServer());
		ServerWorld serverWorld = Objects.requireNonNull(server.getWorld(key));

		// make bed unoccupied before leaving
		var world = player.getWorld();
		var pos = player.getBlockPos();
		BlockState state = world.getBlockState(pos);
		if(state.getBlock() instanceof BedBlock)
		{
			world.setBlockState(pos, state.with(BedBlock.OCCUPIED, false), Block.NOTIFY_ALL);
		}

		current.save();
		target.load();

		// teleport and wake up
		pos = target.getSpawnPos();
		player.teleport(serverWorld, pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5, player.getYaw(),
				player.getPitch());
		player.setSleepingPosition(pos);
		player.wakeUp();
	}

	// NBT ========================================================================================================== //

	public void readNbt(NbtCompound nbt)
	{
		dreamData.readNbt(nbt);
		overworldData.readNbt(nbt);
	}

	public void writeNbt(NbtCompound nbt)
	{
		dreamData.writeNbt(nbt);
		overworldData.writeNbt(nbt);
	}
}