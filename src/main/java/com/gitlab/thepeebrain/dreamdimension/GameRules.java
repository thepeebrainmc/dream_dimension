package com.gitlab.thepeebrain.dreamdimension;

import net.fabricmc.fabric.api.gamerule.v1.CustomGameRuleCategory;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.world.GameRules.BooleanRule;
import net.minecraft.world.GameRules.IntRule;
import net.minecraft.world.GameRules.Key;

import static net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory.createBooleanRule;
import static net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory.createIntRule;
import static net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry.register;

public class GameRules
{
	public static final CustomGameRuleCategory CATEGORY;

	public static final Key<BooleanRule> REQUIRE_EMPTY_HANDS;
	public static final Key<IntRule> DREAM_TICKS;

	public static void init() {}

	static
	{
		CATEGORY = new CustomGameRuleCategory(new Identifier(DreamDimension.MOD_ID, "game_rule_category"),
				Text.translatable("key.categories.dream_dimension").formatted(Formatting.BOLD, Formatting.YELLOW));

		REQUIRE_EMPTY_HANDS = register("requireEmptyHands", CATEGORY, createBooleanRule(false));
		DREAM_TICKS = register("dreamTicks", CATEGORY, createIntRule(60, 0, 100));
	}
}
