## **Dream Dimension**

A Fabric mod that sends sleeping players to a superflat creative dimension.

---

- [**Downloads**](https://www.curseforge.com/minecraft/mc-mods/thepeebrain-dream-dimension/files)
- [**Wiki**](https://gitlab.com/thepeebrainmc/dream_dimension/-/wikis/Home)

---

**Required Mods**

- [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api "Fabric API")